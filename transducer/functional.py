from functools import reduce


def compose(*fs):
    """Compose functions right to left.

    compose(f, g, h)(x) -> f(g(h(x)))

    Args:
        *fs: An arg vector of functions to compose. All functions besides
             the rightmost must be unary.

    Returns:
        The composition of the argument functions. The returned
        function will accept the same arguments as the rightmost
        passed in function.
    """
    return reduce(lambda f, g: lambda *args, **kws: f(g(*args, **kws)), fs)


def identity(x):
    return x


def true(*args, **kwargs):
    return True


def false(*args, **kwargs):
    return False
